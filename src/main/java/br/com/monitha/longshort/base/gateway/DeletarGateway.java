package br.com.monitha.longshort.base.gateway;

public interface DeletarGateway<ID> {

	Boolean deletar (ID id);

}
