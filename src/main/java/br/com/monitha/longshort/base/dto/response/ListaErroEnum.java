package br.com.monitha.longshort.base.dto.response;

public enum ListaErroEnum {
	CAMPOS_OBRIGATORIOS,
	DUPLICIDADE,
	NAO_FOI_POSSIVEL_DELETAR,
	REGISTRO_NAO_ENCONTRADO,
	TIMEOUT,
	OUTROS;

}
