package br.com.monitha.longshort.base.gateway;

public interface SalvarGateway<T> {

	T salvar (T t);

}
