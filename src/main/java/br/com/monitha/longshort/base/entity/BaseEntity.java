package br.com.monitha.longshort.base.entity;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * 
 * @author monic
 * Classe responsável por manter a base para todas as outras bases de dominio
 */
public abstract class BaseEntity {
	
	//UUID
	private String id;
	
	private LocalDateTime dataHoraCriacao;
	
	public BaseEntity() {
		this.id = UUID.randomUUID().toString();
		this.dataHoraCriacao = LocalDateTime.now();
	}

	public String getId() {
		return id;
	}

	public LocalDateTime getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseEntity other = (BaseEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
