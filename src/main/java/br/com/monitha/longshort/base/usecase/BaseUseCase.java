package br.com.monitha.longshort.base.usecase;

public interface BaseUseCase<I, O> {
	
	O executar(I input);
	
}
