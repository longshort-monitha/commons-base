package br.com.monitha.longshort.base.dto.response;

/**
 * 
 * @author monic
 * Mantem a base de resposta para todas as requests do UseCase
 */
public abstract class BaseResponse {
	
	private ResponseData response = new ResponseData();

	public ResponseData getResponse() {
		return response;
	}

	public void setResponse(ResponseData response) {
		this.response = response;
	}

}
